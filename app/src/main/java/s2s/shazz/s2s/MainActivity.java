package s2s.shazz.s2s;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;

public class MainActivity extends Activity implements OnClickListener {

	private PublisherInterstitialAd mInterstitialAd;

	private PublisherAdView mAdView;
	private WebView wb;
	private TextView noImage, backtxt, delayTxt;
	protected static final int REQUEST_OK = 1;
	int count = 0;
	int charactersToWord = 0;
	int delay = 10000;
	Runnable runnable;
	Handler handler = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


		// Create the InterstitialAd and set the adUnitId.
		mInterstitialAd = new PublisherInterstitialAd(this);
		// Defined in res/values/strings.xml
		mInterstitialAd.setAdUnitId(getString(R.string.ad_unit_interstitial));

		//showInterstitial();


		((GoogleAnalyticsS2S) getApplication())
				.getTracker(GoogleAnalyticsS2S.TrackerName.APP_TRACKER);

		// Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
		// values/strings.xml.
		mAdView = (PublisherAdView) findViewById(R.id.ad_view);

		// Create an ad request. Check logcat output for the hashed device ID to
		// get test ads on a physical device. e.g.
		// "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
		PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();

		// Start loading the ad in the background.
		mAdView.loadAd(adRequest);

		findViewById(R.id.button1).setOnClickListener(this);
		wb = (WebView) findViewById(R.id.wb);
		noImage = (TextView) findViewById(R.id.noImage);
		backtxt = (TextView) findViewById(R.id.backtxt);
		delayTxt = (TextView) findViewById(R.id.delayTime);
		delayTxt.setText("Delay between words: " + delay / 1000 + " seconds");
		backtxt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Black))
						.setTitle(getString(R.string.app_name)).setMessage("Are you sure you want to Exit?")
						.setPositiveButton("No", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// do nothing
								dialog.dismiss();
							}
						}).setNegativeButton("Yes", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// Exit
								finish();

							}
						}).setIcon(android.R.drawable.ic_dialog_alert)

						.show();
			}
		});
	}



	@Override
	protected void onStart() {
		super.onStart();
		requestInterstitial();
		showInterstitial();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/** Called when leaving the activity */
	@Override
	public void onPause() {
		if (mAdView != null) {
			mAdView.pause();
		}
		super.onPause();
	}


	private void showInterstitial() {
		// Show the ad if it's ready. Otherwise toast and restart the game.
		if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
			mInterstitialAd.show();
		} else {
			Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();
		//	startGame();
		}
	}
	private void requestInterstitial()
	{
		PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
		mInterstitialAd.loadAd(publisherAdRequest);
	}


	/** Called when returning to the activity */
	@Override
	public void onResume() {
		super.onResume();
		if (mAdView != null) {
			mAdView.resume();
		}
	}

	/** Called before the activity is destroyed */
	@Override
	public void onDestroy() {
		if (mAdView != null) {
			mAdView.destroy();
		}
		super.onDestroy();
	}





	@Override
	public void onClick(View v) {
		// set count and charactersToWord to 0 if the new search is commited
		if (handler != null) {
			count = 0;
			charactersToWord = 0;
			// remove handler callbacs if some text is already showing images
			// to not duplicate handlers
			handler.removeCallbacks(runnable);
		}
		Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
		try {
			startActivityForResult(i, REQUEST_OK);
		} catch (Exception e) {
			Toast.makeText(this, "Sorry, Unable To Find Speech Recognizer Engine !.", Toast.LENGTH_LONG).show();

		}

	}

	// setup increase button
	public void increaseDelay(View v) {
		//maximum delay set to 20000ms = 20 seconds
		if (delay < 20000) {
			// increase delay for 1000 ms = 1 second
			delay = delay + 1000;
			//update delay at delay text view
			delayTxt.setText("Delay between words: " + delay / 1000 + " seconds");
		}
	}
	
	public void decreaseDelay(View v) {
		//minimum delay set to 1000ms = 1 second
		if (delay > 1000) {
			// increase delay for 1000 ms = 1 second
			delay = delay - 1000;
			delayTxt.setText("Delay between words: " + delay / 1000 + " seconds");
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_OK && resultCode == RESULT_OK) {
			ArrayList<String> thingsYouSaid = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

			// thingsYouSaid.get(0) gives you the string of words
			// we make the string to the array of words
			// then we can set a picture to each word
			String[] words = thingsYouSaid.get(0).split("\\s+");
			for (int j = 0; j < words.length; j++) {
				// You may want to check for a non-word character before blindly
				// performing a replacement
				// It may also be necessary to adjust the character class
				words[j] = words[j].replaceAll("[^\\w]", "");
			}

			((TextView) findViewById(R.id.textView2)).setText(thingsYouSaid.get(0));

			// run a runnable loop for every word
			// highlight every word
			// and show specific .gif for it
			final String wholeText = thingsYouSaid.get(0);
			final String[] wordss = words;
			handler = new Handler();

			runnable = new Runnable() {
				@Override
				public void run() {
					// Highlighting the current word
					Spannable WordtoSpan = new SpannableString(wholeText);
					WordtoSpan.setSpan(new ForegroundColorSpan(Color.BLUE), charactersToWord, charactersToWord
							+ wordss[count].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					// set highlighted text to textview
					((TextView) findViewById(R.id.textView2)).setText(WordtoSpan);
					// count character of string that will start next word
					charactersToWord = charactersToWord + wordss[count].length() + 1;
					String currentWord = wordss[count];

					// Set a .gif for the current word
					// and count++ to get another word when loop ends
					count++;
					String gif_url = "file:///android_asset/" + currentWord + ".gif";
					Log.d("image is ", gif_url);

					InputStream x = null;
					try {
						x = getAssets().open(currentWord + ".gif");
					} catch (IOException e) {
						e.printStackTrace();
					}
					if (x == null) {
						Log.d("image is ", "there are no image ");
						wb.setVisibility(View.INVISIBLE);
						noImage.setVisibility(View.VISIBLE);
					} else {
						String html = "<html><body><img style=\"width: 100%\"\"height: 150%\" src=\"" + gif_url
								+ "\"></body></html>";
						wb.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
						wb.getSettings().setLoadWithOverviewMode(true);
						wb.getSettings().setUseWideViewPort(true);
						wb.setVisibility(View.VISIBLE);
						noImage.setVisibility(View.GONE);
					}
					// set a delay until next word and .gif in ms now delay is
					// 10000ms it is 10 seconds
					if (count < wordss.length) {
						handler.postDelayed(runnable, delay);
						// start over again if all words shown
						// set count and charactersToWord to start from first
						// word and picture
						// set delay here too now the delay is 10000ms it is 10
						// seconds
					} else {
						count = 0;
						charactersToWord = 0;
						handler.postDelayed(runnable, delay);
					}
				}
			};
			handler.post(runnable);
		}

	}

	public void onBackPressed()
	{
		if(count == 1)
		{
			count=0;
			this.finish();
			GoogleAnalytics.getInstance(this).reportActivityStop(this);
			System.exit(0);
			moveTaskToBack(true);
		}
		else
		{
			Toast.makeText(getApplicationContext(),"Press back again to exit", Toast.LENGTH_LONG).show();
			count++;
		}

		return;
	}


}
