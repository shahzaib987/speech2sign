package s2s.shazz.s2s;

import android.graphics.Bitmap;

public class ImageClass {
	private Bitmap bmp;
	private String name;
	

	public ImageClass(Bitmap b, String n, int k) {
		bmp = b;
		name = n;
		
	}

	public Bitmap getBitmap() {
		return bmp;
	}

	public String getName() {
		return name;
	}

	}
