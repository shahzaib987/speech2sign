package s2s.shazz.s2s;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DBhelper {
	public static final String IMAGE_ID = "id";
	public static final String IMAGE_NAME = "name";
	//public static final String EMP_AGE = "age";
	public static final String IMAGE_PHOTO = "photo";

	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;

	private static final String DATABASE_NAME = "ImageDb.db";
	private static final int DATABASE_VERSION = 1;

	private static final String SIGN_TABLE = "signTable";
	
	private static final String CREATE_SIGN_TABLE = "create table "
			+ SIGN_TABLE + " (" + IMAGE_ID
			+ " integer primary key autoincrement, " + IMAGE_PHOTO
			+ " blob not null, " + IMAGE_NAME + " text not null unique " + ");";
	

	private final Context mCtx;

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			
			
		}

		

		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_SIGN_TABLE);
			Log.i("DATABASE CREATED !", CREATE_SIGN_TABLE);
			
			
		}

		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + SIGN_TABLE);
			onCreate(db);
		}
	}

	public void Reset() {
		mDbHelper.onUpgrade(this.mDb, 1, 1);
	}

	public DBhelper(Context ctx) {
		mCtx = ctx;
		mDbHelper = new DatabaseHelper(mCtx);
	}

	public DBhelper open() throws SQLException {
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	public void insertImage(ImageClass image) {
		ContentValues cv = new ContentValues();
		cv.put(IMAGE_PHOTO, Utility.getBytes(image.getBitmap()));
		cv.put(IMAGE_NAME, image.getName());
		mDb.insert(SIGN_TABLE, null, cv);
		
		}

/*	
	public void showImage(String img_name) {
		
		mDb = mDbHelper.getWritableDatabase();
		String showQuery = "SELEam sCT * FROM SIGN_TABLE where IMAGE_NAME='"+ img_name +"'";
		Log.d("query",showQuery);		
		mDb.execSQL(showQuery);
	}
	*/
	
	
	public ImageClass retriveImage() throws SQLException {
		
		//String sql = " SELECT * FROM Table WHERE xColumn LIKE '%"+textView1.getText().toString()+"%'";
		
		//cursor = db.rawQuery(sql, null); 
		
		
		
		
		
		Cursor cur = mDb.query(true, SIGN_TABLE, new String[] { IMAGE_PHOTO , IMAGE_NAME}, null, null,null, null, null, null);
		
			if (cur.moveToFirst()) {
			byte[] blob = cur.getBlob(cur.getColumnIndex(IMAGE_PHOTO));
			String name = cur.getString(cur.getColumnIndex(IMAGE_NAME));
			cur.close();
			return new ImageClass(Utility.getPhoto(blob), name, 0);
		}
			
		//	mDb.rawQuery("SELECT * FROM SIGN_TABLE " + "WHERE IMAGE_NAME='"++"';", null); 
			
			
		cur.close();
		return null;
	}
}
